public class Room {
    private final int RoomNumber;
    private final float Price;
    private boolean IsActive;
    
    public Room(int roomNumber, float price) {
        
        RoomNumber = roomNumber;
        Price = price;
        IsActive = true;
    }
    
    public int getRoomNumber() {
        return RoomNumber;
    }
    
    public float getPrice() {
        return Price;
    }
    
    public boolean getIsActive() {
        return IsActive;
    }
    
    public void setIsActive(boolean isActive) {
        IsActive = isActive;
    }
}
