public class Worker extends  Resident {
    private final String Profession;
    
    public Worker(String name, int age, Room room, String profession) {
        super(name, age, room);
        Profession = profession;
    }
    
    public String getProfession() {
        return Profession;
    }
    
    @Override
    public String kratInfo() {
        return "Имя - " + Name + ", профессия " + Profession;
    }

    @Override
    public String fullInfo() {
        return "Имя - " + Name + ", профессия " + Profession + ", возраст " + Age + ", номер комнаты " + Room.getRoomNumber();
    }
}
