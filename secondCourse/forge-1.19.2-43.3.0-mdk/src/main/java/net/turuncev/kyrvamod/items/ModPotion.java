package net.turuncev.kyrvamod.items;

import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.item.alchemy.Potion;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.turuncev.kyrvamod.KyrvaMod;

public abstract class ModPotion {
    public static final DeferredRegister<Potion> POTIONS = DeferredRegister.create(ForgeRegistries.POTIONS, KyrvaMod.MOD_ID);
    public static final RegistryObject<Potion> PORCHA = POTIONS.register("porcha",
            () -> new Potion(new MobEffectInstance(MobEffects.BLINDNESS, 600), new MobEffectInstance(MobEffects.MOVEMENT_SLOWDOWN, 600)));

    public static void register(IEventBus eventBus) {
        POTIONS.register(eventBus);
    }
}
