package net.turuncev.kyrvamod.blocks;

import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Tiers;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.FallingBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.common.ToolAction;
import net.minecraftforge.common.ToolActions;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.turuncev.kyrvamod.KyrvaMod;

public class ModBlock {
    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, KyrvaMod.MOD_ID);
    
    public static final RegistryObject<Block> DRAWING = BLOCKS.register("drawing_table", () -> new FallingBlock(BlockBehaviour.Properties.of(Material.WOOD)
            .strength(1f).requiresCorrectToolForDrops().friction(10)));

    public static final RegistryObject<Block> CUSTOM_LAPIS_BLOCK = BLOCKS.register("custom_lapis", () -> new Block(BlockBehaviour.Properties.of(Material.HEAVY_METAL)
            .strength(3f).requiresCorrectToolForDrops()));
    
    public static final RegistryObject<Block> LAVA_ORE = BLOCKS.register("lava_ore", () -> new Block(BlockBehaviour.Properties.of(Material.STONE)
            .strength(4f).requiresCorrectToolForDrops()));
    
    public static final RegistryObject<Block> GOLDEN_BRICKS = BLOCKS.register("golden_bricks", () -> new Block(BlockBehaviour.Properties.of(Material.BUBBLE_COLUMN)
            .strength(4f).requiresCorrectToolForDrops()));
    
    public static final RegistryObject<Block> PORCELAIN_BRICKS = BLOCKS.register("porcelain_bricks", () -> new Block(BlockBehaviour.Properties.of(Material.METAL)
            .strength(4f).requiresCorrectToolForDrops()));

    public static void register(IEventBus eventBus) {
        BLOCKS.register(eventBus);
    }
}
