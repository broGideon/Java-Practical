package net.turuncev.kyrvamod.items;
import net.minecraft.world.food.FoodProperties;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import net.turuncev.kyrvamod.KyrvaMod;
import net.turuncev.kyrvamod.blocks.ModBlock;


public class ModItem {
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, KyrvaMod.MOD_ID);

    public static final RegistryObject<Item> COIN_ITEM = ITEMS.register("coin", () -> new Item(new Item.Properties().tab(CreativeModeTab.TAB_MISC)));

    public static final RegistryObject<Item> PLATE_FOOD = ITEMS.register("platemeat", () -> new Item(new Item.Properties()
            .food((new FoodProperties.Builder().nutrition(5).saturationMod(0.5f))
                    .build()).tab(CreativeModeTab.TAB_FOOD)));

    public static final RegistryObject<Item> FOOD = ITEMS.register("meatbone", () -> new Item(new Item.Properties()
            .food((new FoodProperties.Builder().nutrition(1).saturationMod(0.2f))
                    .build()).tab(CreativeModeTab.TAB_FOOD)));
    
    public static final RegistryObject<Item> DRAWING = ITEMS.register("drawing_table", () -> new BlockItem(ModBlock.DRAWING.get(), new Item.Properties().tab(CreativeModeTab.TAB_BUILDING_BLOCKS)));
    
    public static final RegistryObject<Item> CUSTOM_LAPIS = ITEMS.register("custom_lapis", () -> new BlockItem(ModBlock.CUSTOM_LAPIS_BLOCK.get(), new Item.Properties().tab(CreativeModeTab.TAB_BUILDING_BLOCKS)));
    
    public static final RegistryObject<Item> LAVA_ORE = ITEMS.register("lava_ore", () -> new BlockItem(ModBlock.LAVA_ORE.get(), new Item.Properties().tab(CreativeModeTab.TAB_DECORATIONS)));
    
    public static final RegistryObject<Item> GOLDEN_BRICKS = ITEMS.register("golden_bricks", () -> new BlockItem(ModBlock.GOLDEN_BRICKS.get(), new Item.Properties().tab(CreativeModeTab.TAB_BUILDING_BLOCKS)));
    
    public static final RegistryObject<Item> PORCELAIN_BRICKS = ITEMS.register("porcelain_bricks", () -> 
            new BlockItem(ModBlock.PORCELAIN_BRICKS.get(), new Item.Properties().tab(CreativeModeTab.TAB_BUILDING_BLOCKS)));
    
    public static void register(IEventBus eventBus) {
        ITEMS.register(eventBus);
    }
}
