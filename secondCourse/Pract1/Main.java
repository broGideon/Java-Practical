package org.example;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);    //запуск потока ввода данных с клавиатуры
        System.out.print("Сколько студентов вы хотите ввести: ");   //указываем пользователю что ему необходимо ввести
        int colvo = in.nextInt();   //считываем введенное пользователем число

        for(int i = 0; colvo > i; i++) {    //запускаем цикл, который будет выполняться выбранное пользователем число раз
            System.out.print("Введите имя: ");
            in.nextLine();
            String name = in.nextLine();
            System.out.print("Введите возраст: ");
            short age = in.nextShort();
            System.out.print("Введите средний балл: ");
            double scores = in.nextDouble();

            FileWriter writer = new FileWriter("Students.txt", true);   //создаем объект класса FileWriter для записи данных в файл
            String text = String.format("Имя: %s, возраст: %d, средний балл: %f\n", name, age, scores); //создаем строку,которую будем записывать в файл
            writer.write(text); //записываем строку в файл

            writer.close(); //закрываем поток для сохранения изменений
        }
    }

}