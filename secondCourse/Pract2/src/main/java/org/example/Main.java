package org.example;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Широта первой точки: ");
        float shir1 = (float) Math.toRadians(sc.nextFloat());
        System.out.print("Долгота первой точки: ");
        float dol1 = (float) Math.toRadians(sc.nextFloat());
        System.out.print("Широта второй точки: ");
        float shir2 = (float) Math.toRadians(sc.nextFloat());
        System.out.print("Долгота второй точки: ");
        float dol2 = (float) Math.toRadians(sc.nextFloat());
        final float RADIUS = 6371F;
        float s = (float) (RADIUS * 2 * Math.asin(Math.sqrt(Math.pow(Math.sin((shir2-shir1)/2), 2) + Math.pow(Math.sin((dol2-dol1)/2), 2) * Math.cos(shir1) * Math.cos(shir2))));
        String result = String.format("%.2f", s);
        System.out.println("Расстояние = " + result + " км");
    }
}