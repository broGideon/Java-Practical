import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.logging.*;

public class Log {
    public Logger logger;
    FileHandler fileHandler;
    
    public Log(String fileName, String nameLogger) throws SecurityException, IOException {
        File file = new File(fileName);
        fileHandler = new FileHandler(fileName, true);
        logger = Logger.getLogger(nameLogger);
        logger.addHandler(fileHandler);
        logger.setLevel(Level.INFO);
        SimpleFormatter formatter = new SimpleFormatter() {
            private static final String format = "{%1$tF %1$tT} [%2$-9s] %3$s %n";
            @Override
            public synchronized String format(LogRecord record) {
                return String.format(format, 
                        new Date(record.getMillis()), 
                        record.getLevel().getName(), 
                        record.getMessage());
            }
        };
        fileHandler.setFormatter(formatter);
    }
}
