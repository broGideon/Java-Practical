import java.io.IOException;
import java.util.Scanner;

public class Kazna{
    private static final Log _log;
    static {
        try {
            _log = new Log("Kazna.log", "Kazna");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    private float _money;

    Scanner sc = new Scanner(System.in);
    
    public void interactionKazna () throws IOException {
        int menu = 0;
        
        do {
            System.out.println("Выберите действие: \n1. Пополнить казну \n2. Потратить золото \n3. Посмотреть запасы золота \n4. Выход");
            try {
                menu = sc.nextInt();
            } catch (Exception e) {
                _log.logger.warning("Вызвано исключение: " + e);
                sc.nextLine();
            }
            
            switch (menu) {
                case 1:
                    deposit();
                    break;
                case 2:
                    withdraw();
                    break;
                case 3:
                    getBalance();
                    break;
                case 4:
                    System.out.println();
                    break;
                default:
                    System.out.println("Такого действя нет");
                    break;
            }
        } while (menu != 4);
    }

    private void deposit() throws IOException{
        try {

            System.out.println("Введите сумму пополения: ");
            float deposit = sc.nextFloat();

            if (deposit > 0) {
                _money += deposit;
            } else {
                System.out.println("Сумма должна быть больше нуля");
            }
        }
        catch (Exception e) {
            _log.logger.warning("Вызвано исключение: " + e);
        } finally {
            sc.nextLine();
        }
    }

    private void withdraw() throws IOException{
        try {

            System.out.println("Введите сколько вам нужно потратить");
            float withdraw = sc.nextFloat();

            if (withdraw <= _money && withdraw > 0) {
                _money -= withdraw;
            } else if (withdraw > _money) {
                System.out.println("У вас недостаточно денег в казне");
            } else if (withdraw < 0) {
                System.out.println("Симма должна быть больше нуля");
            }
        } catch (Exception e) {
            _log.logger.warning("Вызвано исключение: " + e);
        } finally {
            sc.nextLine();
        }
    }
    
    private void getBalance(){
        System.out.println("В казне " + _money + " золота");
    }
}
