import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class MedievalCastle {
    private static final ArrayList<Room> rooms = new ArrayList<>();

    private static final Log _log;
    static {
        try {
            _log = new Log("MedievalCastle.log", "MedievalCastle");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static void start() throws IOException {
        Scanner sc = new Scanner(System.in);
        int menu = 0;
        Taverna taverna = new Taverna();
        
        
        
        Kazna kazna = new Kazna();
        ArrayList<Resident> residents = new ArrayList<>();
        ArrayList<Worker> workers = new ArrayList<>();
        
        do {
            System.out.println("Выберите действие: \n1. Управлять таверной " +
                    "\n2. Управлять казной \n3. Создать комнату \n4. Создать жителя " +
                    "\n5. Сходить в таверну \n6. Создать работника " +
                    "\n7. Вывести всех работников \n8. Выход");
            try {
                menu = sc.nextInt();
            } catch (Exception e) {
                _log.logger.warning("Вызвано исключение: " + e);
                sc.nextLine();
            }
            
            switch (menu) {
                case 1:
                    taverna.interactionTaverna();
                    break;
                case 2:
                    kazna.interactionKazna();
                    break;
                case 3:
                    System.out.println("Введите цену аренды");
                    float price;
                    try {
                        price = sc.nextFloat();
                    } catch (Exception e) {
                        _log.logger.warning("Вызвано исключение: " + e);
                        break;
                    }
                    if (price > 0) {
                        Room room = new Room(rooms.size()+1, price);
                        rooms.add(room);
                    }
                    else {
                        System.out.println("Цена должна быть больше нуля");
                    }
                    break;
                case 4:
                    Resident res = createResident();
                    if (res != null) {
                        residents.add(res);
                        System.out.println("Новый гражданин создан");
                    }
                    break;
                case 5:
                    if (residents.isEmpty()) {
                        System.out.println("У вас нет граждан");
                        break;
                    }
                    for (int i = 1; i <= residents.size(); i++) {
                        System.out.println(i + " " + residents.get(i-1).kratInfo());
                    }
                    int count;
                    
                    try {
                        count = sc.nextInt();
                    } catch (Exception e) {
                        _log.logger.warning("Вызвано исключение: " + e);
                        break;
                    }
                    
                    if (count >= 1 && count <= residents.size()) {
                        taverna.tavern(residents.get(count-1));
                    }
                    else {
                        System.out.println("Такого жителя нет");
                    }
                    break;
                case 6:
                    System.out.println("Введите профессию: ");
                    sc.nextLine();
                    String profession = sc.nextLine();
                    Resident resident = createResident();
                    
                    if (resident == null) {
                        break;
                    }
                    
                    workers.add(new Worker(resident.getName(),resident.getAge(), resident.getRoom(), profession));
                    System.out.println("Работник " + workers.getLast().kratInfo() + " добавлен");
                    break;
                case 7:
                    for (Worker _worker : workers) {
                        try {
                            System.out.println(_worker.fullInfo());
                        }
                        catch (Exception e) {
                            System.out.println(_worker.kratInfo());
                        }
                    }
                    break;
                case 8:
                    System.out.println();
                    break;
                default:
                    System.out.println("Такого действия нет");
            }
        } while (menu != 8);
    } 
    
    private static Resident createResident() {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Введите имя: ");
        String name = sc.nextLine();

        System.out.println("Введите возраст жителя: ");
        int age;
        try {
            age = sc.nextInt();
        } catch (Exception e) {
            _log.logger.warning("Вызвано исключение: " + e);
            sc.nextLine();
            return null;
        }

        if (age <= 0) {
            System.out.println("Возраст должен быть больше нуля");
            return null;
        }

        System.out.println("Выберите комнату жителя: ");

        if(rooms.isEmpty()) {
            System.out.println("Комнат нет");
            return null;
        }
        int i = 1;
        
        for (Room item : rooms) {
            System.out.println(i + ". Номер: " + item.getRoomNumber() + ", цена - " + item.getPrice() + ", доступно ли жилье: " + item.getIsActive());
            i++;
        }

        int roomNum;
        try {
            roomNum = sc.nextInt();
        } catch (Exception e) {
            _log.logger.warning("Вызвано исключение: " + e);
            sc.nextLine();
            return null;
        }

        if (roomNum >= 1 && rooms.get(roomNum-1).getIsActive() && roomNum <= rooms.size()) {
            Room room_ = rooms.get(roomNum-1);
            room_.setIsActive(false);
        }
        else if (roomNum < 1 || roomNum > rooms.size()) {
            System.out.println("Некоректный номер комнаты");
            return null;
        }
        else {
            System.out.println("Комната уже занята");
            return null;
        }
        
        return new Resident(name, age, rooms.get(roomNum-1));
    }
}
