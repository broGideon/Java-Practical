import java.io.IOException;

public class Main {
    private static final Log _log;
    static {
        try {
            _log = new Log("main.log", "Main");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static void main(String[] args) throws IOException {
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
        {
            @Override
            public void uncaughtException(Thread t, Throwable e)
            {
                _log.logger.severe("Возникло необработанное исключение: " + e.toString());
            }
        });
        
        MedievalCastle.start();
    }
}

