import java.io.IOException;

public class Resident {
     protected String Name;
     protected int Age;
     protected Room Room;

    private static final Log _log;
    static {
        try {
            _log = new Log("Resident.log", "Resident");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
     
     public Resident(String name, int age, Room room) {
         Name = name;
         Age = age;
         Room = room;
         _log.logger.info("Создан гражданин " + name);
     }
     
    public String kratInfo(){
         return "Имя - " + Name + ", возраст: " + Age;
    }
    
    public String fullInfo() {
         return "Имя - " + Name + ", возраст " + Age + ", номер комнаты " + Room.getRoomNumber() + ", цена" + Room.getPrice();
    }
    
    public Room getRoom(){
         return Room;
    }
    
    public int getAge(){
         return Age;
    }
    
    public String getName(){
         return Name;
    }
}
