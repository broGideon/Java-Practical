import java.io.IOException;

public class Worker extends  Resident {
    private final String Profession;

    private static final Log _log;
    static {
        try {
            _log = new Log("Worker.log", "Worker");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    public Worker(String name, int age, Room room, String profession) {
        super(name, age, room);
        Profession = profession;
        _log.logger.info("Создан работник " + name);
    }
    
    public String getProfession() {
        return Profession;
    }
    
    @Override
    public String kratInfo() {
        return "Имя - " + Name + ", профессия " + Profession;
    }

    @Override
    public String fullInfo() {
        return "Имя - " + Name + ", профессия " + Profession + ", возраст " + Age + ", номер комнаты " + Room.getRoomNumber();
    }
}
