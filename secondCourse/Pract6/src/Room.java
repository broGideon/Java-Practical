import java.io.IOException;

public class Room {
    private final int RoomNumber;
    private final float Price;
    private boolean IsActive;
    private static Log _log;
    static {
        try {
            _log = new Log("Room.log", "Room");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public Room(int roomNumber, float price) throws IOException {
        RoomNumber = roomNumber;
        Price = price;
        IsActive = true;

        _log.logger.info("Комната создана");
    }
    
    public int getRoomNumber() {
        return RoomNumber;
    }
    
    public float getPrice() {
        return Price;
    }
    
    public boolean getIsActive() {
        return IsActive;
    }
    
    public void setIsActive(boolean isActive) {
        IsActive = isActive;
    }
}
