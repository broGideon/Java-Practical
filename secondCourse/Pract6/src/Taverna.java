import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Taverna {
     private boolean _isOpen = false;
     private int _storage;
     
    private static final Log _log;
    static {
        try {
            _log = new Log("Taverna.log", "Taverna");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    Scanner sc = new Scanner(System.in);
    public void interactionTaverna() {
        int menu = 0;
        
        do {
            System.out.println("Выберите действие: \n1. Открыть таверну \n2. Закрыть таверну \n3. Пополнить склад \n4. Посмотреть склад \n5. Выход");
            try {
                menu = sc.nextInt();
            } catch (Exception e) {
                _log.logger.warning("Вызвано исключение: " + e);
                sc.nextLine();
            }
            
            switch (menu) {
                case 1:
                    openTavern();
                    break;
                case 2:
                    closeTavern();
                    break;
                case 3:
                    addStorage();
                    break;
                case 4:
                    getStorage();
                    break;
                case 5:
                    System.out.println();
                    break;
                default:
                    System.out.println("Такого действия нет");
            }
        } while (menu != 5);
    }

    private void openTavern(){
        if (!_isOpen) {
            _isOpen = true;
            System.out.println("Таверна открыта");
            _log.logger.info("Таверна открыта");
        }
        else {
            System.out.println("Таверна и так открыта");
        }
    }

    private void closeTavern(){
        if (_isOpen) {
            _isOpen = false;
            System.out.println("Таверна закрыта");
            _log.logger.info("Таверна закрыта");
        }
        else {
            System.out.println("Таверна и так закрыта");
        }
    }
    
    private void addStorage(){
        System.out.println("Введите сколоко вы хотите пополнить");
        int count;
        try {
            count = sc.nextInt();
        } catch (Exception e) {
            _log.logger.warning("Вызвано исключение: " + e);
            sc.nextLine();
            return;
        }
        if (count > 0) {
            _storage += count;
            System.out.println("Вы пополнили склад");
            _log.logger.info("Склад пополнен на " + _storage);
        }
        else {
            System.out.println("Количество должно быть больше нуля");
        }
        
        sc.nextLine();
    }
    
    private void getStorage(){
        System.out.println("На складе " + _storage + " бутылок пива");
    }
    
    public void tavern(Resident resident) {
        if (resident.Age < 18) {
            System.out.println("Возраст должен быть больше 18");
            return;
        }
        
        if (!_isOpen) {
            System.out.println("Таверна закрыта");
            return;
        }
        
        if (_storage == 0) {
            System.out.println("На складе нет выпивки");
            return;
        }

        _storage--;
        System.out.println(resident.Name + " выпил одно пиво");
    }
}
