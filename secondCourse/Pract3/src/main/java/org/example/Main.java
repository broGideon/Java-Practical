package org.example;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Введите сторону a: ");
        int a = sc.nextInt();
        System.out.print("Введите сторону b: ");
        int b = sc.nextInt();
        System.out.print("Введите сторону c: ");
        int c = sc.nextInt();

        if ((a + b <= c || b + c <= a || c + a <= b)) {
            System.out.println("a, b и c не являются сторонами треугольника");
        }
        else if ((a * a) + (b * b) == c * c || (a * a) + (c * c) == b * b || (c * c) + (b * b) == a * a) {
            System.out.println("a, b и c являются сторонами прямоугольного треугольника");
        }
        else if (a == b && b == c) {
            System.out.println("a, b и c являются сторонами равностороннего треугольника");
        }
        else if (a == b || b == c || c == a) {
            System.out.println("a, b и c являются сторонами равнобедренного треугольника");
        }
        else {
            System.out.println("a, b и c являются сторонами обычного треугольника");
        }
    }
}