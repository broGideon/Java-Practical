package com.example.a15pract;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class SecondFragment extends Fragment {

    private Button button;
    private int counter;

    @Nullable
    @NonNull
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanseState){
        View view = inflater.inflate(R.layout.fragment_second, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        button = view.findViewById(R.id.button);
        button.setBackgroundColor(getResources().getColor(R.color.perpel));
        button.setOnClickListener(this::showPopupMenu);
    }

    private void showPopupMenu(View v) {
        PopupMenu popupMenu = new PopupMenu(getContext(), v);
        popupMenu.inflate(R.menu.popup_menu);
        MenuItem counterItem = popupMenu.getMenu().findItem(R.id.counter);
        counterItem.setTitle("Количество открытий: " + ++counter);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int itemId = item.getItemId();
                        if (itemId == R.id.first_button_color) {
                            button.setBackgroundColor(getResources().getColor(R.color.red));
                            return true;
                        } else if (itemId == R.id.second_button_color) {
                            button.setBackgroundColor(getResources().getColor(R.color.perpel));
                            return true;
                        }
                        return false;
                    }
                });
        popupMenu.show();
    }
}
