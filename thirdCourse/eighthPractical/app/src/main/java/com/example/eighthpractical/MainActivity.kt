package com.example.eighthpractical

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

class MainActivity : AppCompatActivity() {
    private lateinit var dateTimeText: TextView
    private var selectedService: Service? = null
    private lateinit var services: MutableList<Service>
    private lateinit var servicesFiltered: MutableList<Service>
    private lateinit var adapter: RecyclerViewAdapter<Service>
    private var db = FirebaseFirestore.getInstance()
    private var auth = FirebaseAuth.getInstance()
    private var selectedDate: String = ""
    private var selectedTime: String = ""
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        dateTimeText = findViewById(R.id.dateTimeText)
        services = mutableListOf()
        servicesFiltered = mutableListOf()
        
        findViewById<EditText>(R.id.searchEditText).addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.let { 
                    val query = it.toString()
                    servicesFiltered.clear()
                    servicesFiltered.addAll(if (query.isEmpty()) {
                        services
                    } else {
                        services.filter { it.name.contains(query, ignoreCase = true) }.toMutableList()
                    })
                    adapter.notifyDataSetChanged()
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })

        val binder = object : ViewBinder<Service> {
            override fun bindView(item: Service, itemView: View) {
                val textView = itemView.findViewById<TextView>(R.id.nameService)
                textView.text = item.name
                itemView.setOnClickListener {
                    selectedService = item
                    showDateTimePicker()
                }
            }
        }

        val recyclerView: RecyclerView = findViewById(R.id.serviceRecyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(servicesFiltered, binder, R.layout.service_card)
        recyclerView.adapter = adapter
        
        findViewById<Button>(R.id.confirmButton).setOnClickListener {
            if (selectedService == null || selectedDate.isEmpty() || selectedTime.isEmpty()) {
                Toast.makeText(this, "Выберите сервис, дату и время", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val user = auth.currentUser
            if (user == null) {
                Toast.makeText(this, "Пользователь не автоизован", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val appointment = Appointment(
                clientId = user.uid,
                clientName = user.email ?: "",
                serviceId = selectedService?.id.toString(),
                serviceName = selectedService?.name.toString(),
                date = selectedDate,
                time = selectedTime
            )

            db.collection("appointments").add(appointment)
                .addOnSuccessListener {
                    Toast.makeText(this, "Запись успешно создана", Toast.LENGTH_SHORT).show()
                    selectedService = null
                    selectedDate = ""
                    selectedTime = ""
                    dateTimeText.text = "Выбранные дата и время"
                }
                .addOnFailureListener { onFailure(it) }
        }
        
        loadData()
    }

    private fun showDateTimePicker() {
        val calendar = Calendar.getInstance()
        val datePickerDialog = DatePickerDialog(this, { _, year, month, dayOfMonth ->
            val timePickerDialog = TimePickerDialog(this, { _, hourOfDay, minute ->
                val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())
                calendar.set(year, month, dayOfMonth, hourOfDay, minute)
                selectedDate = formatter.format(calendar.time)
                selectedTime = "$hourOfDay:$minute"
                dateTimeText.text = "Выбранные дата и время: $selectedDate $selectedTime"
            }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true)
            timePickerDialog.show()
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
        datePickerDialog.show()
    }

    private fun loadData() {
        db.collection("service").get()
            .addOnSuccessListener { querySnapshot ->
                servicesFiltered.clear()
                servicesFiltered.addAll(querySnapshot.toObjects(Service::class.java))
                services.addAll(servicesFiltered)
                adapter.notifyDataSetChanged()
            }
            .addOnFailureListener { onFailure(it) }
    }

    private fun onFailure(ex: Exception) {
        Toast.makeText(this, "Ошибка: ${ex.message}", Toast.LENGTH_SHORT).show()
    }
}