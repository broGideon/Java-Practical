package com.example.eighthpractical

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore


class AuthActivity : AppCompatActivity() {
    private lateinit var emailField: EditText
    private lateinit var passwordField: EditText
    private lateinit var loginBtn: Button
    private lateinit var registerBtn: Button
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_auth)
        
        auth = FirebaseAuth.getInstance()
        emailField = findViewById(R.id.emailField)
        passwordField = findViewById(R.id.passwordField)
        loginBtn = findViewById(R.id.loginButton)
        registerBtn = findViewById(R.id.registerButton)
        
        loginBtn.setOnClickListener{loginUser()}
        
        registerBtn.setOnClickListener{registerUser()}
    }
    
    private fun registerUser() {
        val email = emailField.text.toString()
        val password = passwordField.text.toString()
        
        if(email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Заполните все поля", Toast.LENGTH_SHORT).show()
            return
        }
        if (password.length < 6) {
            Toast.makeText(this, "Пароль менее 6 символов", Toast.LENGTH_SHORT).show()
            return
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Toast.makeText(this, "Неверный формат почты", Toast.LENGTH_SHORT).show()
            return
        }
        
        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                Toast.makeText(this, "Регистрация прошла успешно", Toast.LENGTH_SHORT).show()
                saveUserToFirestore(email, password)
            }
            else {
                if (task.exception !== null) {
                    val error = task.exception!!.message
                    Toast.makeText(this, "Ошибка $error", Toast.LENGTH_SHORT).show()
                    Log.e("AuthError", "Ошибка регистрации", task.exception)
                }
            }
                
        }
    }


    private fun loginUser() {
        val email = emailField.text.toString()
        val password = passwordField.text.toString()
        if (email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Заполните все поля", Toast.LENGTH_SHORT).show()
            return
        }
        
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this) { task ->
            if(task.isSuccessful)
                checkUserRole()
            else
                Toast.makeText(this, "Ошибка авторизации", Toast.LENGTH_SHORT).show()
            
        }
    }
    
    private fun saveUserToFirestore(email: String, password: String) {
        val db = FirebaseFirestore.getInstance()
        val user = User(email, password, "user")
        user.id = auth.currentUser?.uid ?: "12345"
        db.collection("users").document(auth.currentUser?.uid ?: "12345").set(user).addOnSuccessListener {
            Toast.makeText(
                this,
                "Данные пользователя успешно сохранены",
                Toast.LENGTH_SHORT
            ).show()
            startActivity(Intent(this@AuthActivity, MainActivity::class.java))
            finish()
        }.addOnFailureListener { e ->
            Toast.makeText(this, "Данные пользователя не сохранены" + e.message, Toast.LENGTH_SHORT).show();
        }
    }
    
    private fun checkUserRole() {
        val user = auth.currentUser
        if (user != null) {
            val db = FirebaseFirestore.getInstance()
            db.collection("users").document(user.uid).get()
                .addOnSuccessListener { documentSnapshot: DocumentSnapshot ->
                    val role = documentSnapshot.getString("role")
                    if (role != null) {
                        when (role) {
                            "admin" -> startActivity(
                                Intent(
                                    this@AuthActivity,
                                    AdminActivity::class.java
                                )
                            )
                            "employee" -> startActivity(
                                Intent(
                                    this@AuthActivity,
                                    EmployeeActivity::class.java
                                )
                            )
                            "user" -> startActivity(
                                Intent(
                                    this@AuthActivity,
                                    MainActivity::class.java
                                )
                            )
                        }
                        finish()
                    }
                }
        }
    }
}