package com.example.eighthpractical

import android.view.View

interface ViewBinder<T> {
    fun bindView(item: T, itemView: View)
}