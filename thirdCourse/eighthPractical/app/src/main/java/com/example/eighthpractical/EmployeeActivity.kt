package com.example.eighthpractical

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class EmployeeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_employee)
        val serviceBtn: Button = findViewById(R.id.serviceEmployeeBtn)
        val exitBtn: Button = findViewById(R.id.exitEmployeeBtn)

        serviceBtn.setOnClickListener {
            startActivity(
                Intent(
                    this@EmployeeActivity, ServiceActivity::class.java
                )
            )
            finish()
        }

        exitBtn.setOnClickListener {
            startActivity(
                Intent(
                    this@EmployeeActivity, AuthActivity::class.java
                )
            )
            finish()
        }
    }
}