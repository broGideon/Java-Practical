package com.example.eighthpractical

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore
import java.time.LocalDateTime

class ServiceActivity : AppCompatActivity() {
    private val db = FirebaseFirestore.getInstance()
    private val serviceCollection = db.collection("service")
    private val logsCollection = db.collection("logs")
    private lateinit var nameServiceEdit: EditText
    private lateinit var adapter: RecyclerViewAdapter<Service>
    private lateinit var services: MutableList<Service>
    private var selectId: String = ""
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_service)
        services = mutableListOf()
        
        val binder = object : ViewBinder<Service> {
            override fun bindView(item: Service, itemView: View) {
                val textView = itemView.findViewById<TextView>(R.id.nameService)
                textView.text = item.name
                itemView.setOnClickListener {
                    selectId = item.id
                    nameServiceEdit.setText(item.name)
                }
            }
        }

        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(services, binder, R.layout.service_card)
        recyclerView.adapter = adapter
        nameServiceEdit = findViewById(R.id.emailEdit)
        findViewById<Button>(R.id.addBtn).setOnClickListener { addService() }
        findViewById<Button>(R.id.updateBtn).setOnClickListener { updateService() }
        findViewById<Button>(R.id.delBtn).setOnClickListener { delService() }
        loadData()
    }
    
    private fun addService() {
        val name = nameServiceEdit.text.toString()
        if (name.isEmpty()) {
            return
        }
        
        val service = Service()
        service.name = name
        
        serviceCollection.add(service)
            .addOnSuccessListener { documentReference -> 
                service.id = documentReference.id
                documentReference.update("id", documentReference.id)
                    .addOnSuccessListener { 
                        services.add(service)
                        writeLog("Добавлен сервис с id = ${service.id}")
                        clearInput()
                        onSuccess() 
                    }
                    .addOnFailureListener { onFailure(it) }
            }
            .addOnFailureListener { onFailure(it) }
    }
    
    private fun updateService() {
        if (selectId.isEmpty()) {
            return
        }
        
        val name = nameServiceEdit.text.toString()
        if (name.isEmpty()) {
            return
        }
        
        val serviceRef = serviceCollection.document(selectId)
        serviceRef.update("name", name)
            .addOnSuccessListener { 
                val service = services.find { it.id == selectId }
                service?.name = name
                writeLog("Сервис с $selectId изменён")
                clearInput()
                onSuccess() 
            }
            .addOnFailureListener { onFailure(it) }
    }

    private fun delService() {
        if (selectId.isEmpty()) {
            return
        }
        
        val serviceRef = serviceCollection.document(selectId)
        serviceRef.delete()
            .addOnSuccessListener {
                val service = services.find { it.id == selectId }
                service?.let { services.remove(it) }
                writeLog("Сервис с $selectId удалён")
                clearInput()
                onSuccess()
            }
            .addOnFailureListener { onFailure(it) }
    }
    
    private fun onSuccess() {
        Toast.makeText(this, "Операция прошла успешно", Toast.LENGTH_SHORT).show()
    }

    private fun onFailure(ex: Exception) {
        Toast.makeText(this, "Ошибка: ${ex.message}", Toast.LENGTH_SHORT).show()
        writeLog("Ошибка: ${ex.message}")
    }
    
    private fun clearInput() {
        nameServiceEdit.setText("")
        adapter.notifyDataSetChanged()
    }
    
    private fun writeLog(msg: String) {
        val log = mutableMapOf<String, Any>()
        log["message"] = msg
        log["time"] = LocalDateTime.now()
        logsCollection.add(log)
    }
    
    private fun loadData() {
        serviceCollection.get()
            .addOnSuccessListener { querySnapshot ->
                services.clear() 
                services.addAll(querySnapshot.toObjects(Service::class.java)) 
                adapter.notifyDataSetChanged()
            }
            .addOnFailureListener { onFailure(it) }
    }
}