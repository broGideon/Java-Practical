package com.example.eighthpractical

class Appointment (
    var clientId: String = "",
    var clientName: String = "",
    var serviceId: String = "",
    var serviceName: String = "",
    var date: String = "",
    var time: String = ""
)