package com.example.eighthpractical

import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.time.LocalDateTime

class UserActivity : AppCompatActivity() {
    private lateinit var roleSpinner: Spinner
    private lateinit var emailEdit: EditText
    private lateinit var passwordEdit: EditText
    private lateinit var selectedRole: String
    private val db = FirebaseFirestore.getInstance()
    private val userCollection = db.collection("users")
    private val logsCollection = db.collection("logs")
    private lateinit var adapter: RecyclerViewAdapter<User>
    private lateinit var users: MutableList<User>
    private lateinit var selectedId: String
    private val auth = FirebaseAuth.getInstance()
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_user)
        emailEdit = findViewById(R.id.emailEdit)
        passwordEdit = findViewById(R.id.passwordEdit)
        users = mutableListOf()
        spinnerInit()

        val binder = object : ViewBinder<User> {
            override fun bindView(item: User, itemView: View) {
                val email = itemView.findViewById<TextView>(R.id.emailTextView)
                val password = itemView.findViewById<TextView>(R.id.passwordTextView)
                val role = itemView.findViewById<TextView>(R.id.roleTextView)
                email.text = item.email
                password.text = item.password
                role.text = item.role
                itemView.setOnClickListener {
                    emailEdit.setText(item.email)
                    passwordEdit.setText(item.password)
                    selectedId = item.id
                    val position = findPositionByValue(item.role)
                    if (position != -1)
                        roleSpinner.setSelection(position)
                }
            }
        }

        val recyclerView: RecyclerView = findViewById(R.id.recyclerViewUser)
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(users, binder, R.layout.user_card)
        recyclerView.adapter = adapter
        findViewById<Button>(R.id.addBtn).setOnClickListener { addUser() }
        findViewById<Button>(R.id.updateBtn).setOnClickListener { updateUser() }
        findViewById<Button>(R.id.delBtn).setOnClickListener { delUser() }
        loadData()
    }


    private fun addUser() {
        val email = emailEdit.text.toString()
        val password = passwordEdit.text.toString()
        if (!validateForm(email, password))
            return
        
        val user = User(email, password, selectedRole)

        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val userId = auth.currentUser?.uid ?: return@addOnCompleteListener
                    user.id = userId
                    userCollection.document(userId).set(user)
                        .addOnSuccessListener {
                            users.add(user)
                            writeLog("Добавлен пользователь с id = ${user.id}")
                            onSuccess() 
                            clearInput()
                        }
                        .addOnFailureListener { onFailure(it) }
                } else {
                    task.exception?.let { onFailure(it) }
                }
            }
    }

    private fun updateUser() {
        if (selectedId.isEmpty())
            return
        if (selectedRole.isEmpty())
            return
        val userRef = userCollection.document(selectedId)
        userRef.update("role", selectedRole)
            .addOnSuccessListener {
                val user = users.find { it.id == selectedId }
                user?.role = selectedRole
                writeLog("Пользователь с $selectedId изменён")
                clearInput()
                onSuccess()
            }
            .addOnFailureListener { onFailure(it) }
    }

    private fun delUser() {
        if (selectedId.isEmpty()) {
            return
        }

        val userRef = userCollection.document(selectedId)
        userRef.delete()
            .addOnSuccessListener {
                val user = users.find { it.id == selectedId }
                user?.let { users.remove(it) }
                writeLog("Пользователь с $selectedId удалён")
                clearInput()
                onSuccess()
            }
            .addOnFailureListener { onFailure(it) }
    }

    private fun onSuccess() {
        Toast.makeText(this, "Операция прошла успешно", Toast.LENGTH_SHORT).show()
    }

    private fun onFailure(ex: Exception) {
        Toast.makeText(this, "Ошибка: ${ex.message}", Toast.LENGTH_SHORT).show()
        writeLog("Ошибка: ${ex.message}")
    }

    private fun clearInput() {
        emailEdit.setText("")
        passwordEdit.setText("")
        roleSpinner.setSelection(1)
        adapter.notifyDataSetChanged()
    }
    
    private fun spinnerInit() {
        roleSpinner = findViewById(R.id.roleSpinner)


        val arrayAdapter = ArrayAdapter.createFromResource(
            this,
            R.array.roles, android.R.layout.simple_spinner_item
        )

        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        roleSpinner.adapter = arrayAdapter


        roleSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View?,
                position: Int,
                id: Long
            ) {
                selectedRole = parent.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }
    
    private fun validateForm(email: String, password: String): Boolean {
        if (email.isEmpty() && password.isEmpty() && selectedRole.isEmpty()) {
            Toast.makeText(this, "Заполните все поля", Toast.LENGTH_SHORT).show()
            return false
        }

        if (password.length < 6) {
            Toast.makeText(this, "Пароль менее 6 символов", Toast.LENGTH_SHORT).show()
            return false
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Toast.makeText(this, "Неверный формат почты", Toast.LENGTH_SHORT).show()
            return false
        }
        
        return true
    }

    private fun findPositionByValue(value: String): Int {
        val stringAdapter = roleSpinner.adapter as ArrayAdapter<*>
        for (i in 0 until stringAdapter.count) {
            if (stringAdapter.getItem(i).toString() == value) {
                return i
            }
        }
        return -1 
    }

    private fun writeLog(msg: String) {
        val log = mutableMapOf<String, Any>()
        log["message"] = msg
        log["time"] = LocalDateTime.now()
        logsCollection.add(log)
    }
    
    private fun loadData() {
        userCollection.get()
            .addOnSuccessListener { querySnapshot ->
                users.clear()
                users.addAll(querySnapshot.toObjects(User::class.java))
                adapter.notifyDataSetChanged()
            }
            .addOnFailureListener { onFailure(it) }
    }
}
