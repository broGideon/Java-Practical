package com.example.eighthpractical

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material3.Button
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class AdminActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_admin)
        val serviceBtn: Button = findViewById(R.id.serviceBtn)
        val userBtn: Button = findViewById(R.id.userBtn)
        val exitBtn: Button = findViewById(R.id.exitBtn)

        serviceBtn.setOnClickListener {
            startActivity(
                Intent(
                    this@AdminActivity, ServiceActivity::class.java
                )
            )
            finish()
        }

        userBtn.setOnClickListener {
            startActivity(
                Intent(
                    this@AdminActivity, UserActivity::class.java
                )
            )
            finish()
        }
        
        exitBtn.setOnClickListener {
            startActivity(
                Intent(
                    this@AdminActivity, AuthActivity::class.java
                )
            )
            finish()
        }
    }
}