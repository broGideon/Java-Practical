package com.example.eighthpractical

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewAdapter<T>(private val listItems: List<T>, private val binder: ViewBinder<T>, private val layoutId: Int): RecyclerView.Adapter<RecyclerViewAdapter<T>.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listItems.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item: T = listItems[position]
         binder.bindView(item, holder.itemView)
    }
        
    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
}