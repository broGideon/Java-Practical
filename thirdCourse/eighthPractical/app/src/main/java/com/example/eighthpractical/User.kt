package com.example.eighthpractical

class User {
    var id: String = ""
    var email: String = ""
    var password: String = ""
    var role: String = ""

    constructor(email: String, password: String, role: String) {
        this.password = password
        this.email = email
        this.role = role
    }

    constructor()
}