package com.example.thirdpractical_thirdcourse;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences themeSettings;
    private ImageButton themeBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        themeSettings = getSharedPreferences("SETTINGS", MODE_PRIVATE);
        setCurrentTheme();

        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);

        themeBtn = findViewById(R.id.theme_button);
        updateImageTheme();
        themeBtn.setOnClickListener(v -> toggleTheme());

        Button playerBtn = findViewById(R.id.player);
        playerBtn.setOnClickListener((v) -> {
            Intent intent = new Intent(MainActivity.this, PlayerActivity.class);
            startActivity(intent);
        });

        Button botBtn = findViewById(R.id.bot);
        botBtn.setOnClickListener((v) -> {
            Intent intent = new Intent(MainActivity.this, BotActivity.class);
            startActivity(intent);
        });
    }

    private void toggleTheme() {
        boolean isNightMode = themeSettings.getBoolean("IS_NIGHT_MODE", false);
        SharedPreferences.Editor settingsEditor = themeSettings.edit();

        if (isNightMode) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            settingsEditor.putBoolean("IS_NIGHT_MODE", false);
            Toast.makeText(MainActivity.this, "Включена светлая тема", Toast.LENGTH_SHORT).show();
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            settingsEditor.putBoolean("IS_NIGHT_MODE", true);
            Toast.makeText(MainActivity.this, "Включена тёмная тема", Toast.LENGTH_SHORT).show();
        }

        settingsEditor.apply();
        updateImageTheme();
    }

    private void updateImageTheme() {
        boolean isNightMode = themeSettings.getBoolean("IS_NIGHT_MODE", false);
        themeBtn.setImageResource(isNightMode ? R.drawable.light : R.drawable.night);
    }

    private void setCurrentTheme() {
        boolean isNightMode = themeSettings.getBoolean("IS_NIGHT_MODE", false);
        AppCompatDelegate.setDefaultNightMode(isNightMode ? AppCompatDelegate.MODE_NIGHT_YES : AppCompatDelegate.MODE_NIGHT_NO);
    }
}
