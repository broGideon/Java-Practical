package com.example.sixpractical;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import io.paperdb.Paper;

public class FullInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_full_info);

        ImageView imageView = findViewById(R.id.imageView);
        TextView titleTextView = findViewById(R.id.titleTextView);
        TextView descriptionTextView = findViewById(R.id.descriptionTextView);

        String id = getIntent().getStringExtra("id");
        Cloth cloth = Paper.book().read(id);

        String imageUrl = cloth.getUrl();
        Glide.with(this)
                .load(imageUrl)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .error("https://via.placeholder.com/600x400")
                .into(imageView);

        // Установка текста
        String title = cloth.getClothName();
        String description = cloth.getClotheDescription();
        titleTextView.setText(title);
        descriptionTextView.setText(description);
    }
}