package com.example.sixpractical;

public class Cloth {
    private String clotheDescription;
    private String clothName;
    private String url;

    public Cloth(String clothName, String clotheDescription, String url) {
        this.clothName = clothName;
        this.clotheDescription = clotheDescription;
        this.url = url;
    }

    public String getClotheDescription() {
        return clotheDescription;
    }

    public void setClotheDescription(String clotheDescription) {
        this.clotheDescription = clotheDescription;
    }

    public String getClothName() {
        return clothName;
    }

    public void setClothName(String clothName) {
        this.clothName = clothName;
    }

    public String getUrl() { return url; }
    public void setUrl(String url) { this.url = url; }
}
