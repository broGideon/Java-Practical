package com.example.secondpractical_thirdcourse;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Dog> dogs = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);

        setInitialData();
        RecyclerView recyclerView = findViewById(R.id.list);

        StateAdapter.OnClickListener onClickListener = new StateAdapter.OnClickListener() {
            @Override
            public void onClick(Dog dog, int position) {
                Intent intent = new Intent(MainActivity.this, FullInfo.class);
                intent.putExtra(Dog.class.getSimpleName(), dog);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.stop_position);
            }
        };

        StateAdapter adapter = new StateAdapter(MainActivity.this, dogs, onClickListener);
        recyclerView.setAdapter(adapter);

    }

    private void setInitialData () {
        dogs.add(new Dog(R.drawable.ovcharka, "Овчарка", "Нужно придумать описание"));
        dogs.add(new Dog(R.drawable.kaif, "Кайфарик", "Нужно придумать описание"));
        dogs.add(new Dog(R.drawable.shabaka, "Шабака", "Нужно придумать описание"));
        dogs.add(new Dog(R.drawable.corgi, "Корги", "Нужно придумать описание"));
        dogs.add(new Dog(R.drawable.corgi2, "Кайфарик корги", "Нужно придумать описание"));
        dogs.add(new Dog(R.drawable.terier, "Терьер", "Нужно придумать описание"));
        dogs.add(new Dog(R.drawable.ovcharka, "Овчарка", "Нужно придумать описание"));
        dogs.add(new Dog(R.drawable.kaif, "Кайфарик", "Нужно придумать описание"));
        dogs.add(new Dog(R.drawable.shabaka, "Шабака", "Нужно придумать описание"));
        dogs.add(new Dog(R.drawable.corgi, "Корги", "Нужно придумать описание"));
        dogs.add(new Dog(R.drawable.corgi2, "Кайфарик корги", "Нужно придумать описание"));
        dogs.add(new Dog(R.drawable.terier, "Терьер", "Нужно придумать описание"));
    }
}