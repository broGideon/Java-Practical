package com.example.secondpractical_thirdcourse;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class FullInfo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_full_info);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView img = findViewById(R.id.image);
        TextView description = findViewById(R.id.description);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Bundle arguments = getIntent().getExtras();
        Dog dog;

        if (arguments != null) {
            dog = (Dog) arguments.getSerializable(Dog.class.getSimpleName());
            toolbar.setTitle(dog.getName());
            img.setImageResource(dog.getImage());
            description.setText(dog.getDescription());
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        overridePendingTransition(R.anim.stop_position, R.anim.slide_out_right);
        return true;
    }
}