package com.example.secondpractical_thirdcourse;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class StateAdapter extends RecyclerView.Adapter<StateAdapter.ViewHolder> {

    interface OnClickListener{
        void onClick(Dog dog, int position);
    }

    private final OnClickListener onClickListener;
    private final LayoutInflater inflater;
    private final ArrayList<Dog> dogs;

    public StateAdapter(Context context, ArrayList<Dog> dogs, OnClickListener onClickListener) {
        this.dogs = dogs;
        this.inflater = LayoutInflater.from(context);
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public StateAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StateAdapter.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        Dog dog = dogs.get(position);
        holder.img.setImageResource(dog.getImage());
        holder.name.setText(dog.getName());
        //holder.description.setText(dog.getDescription());

        holder.itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                onClickListener.onClick(dog, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dogs.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        final ImageView img;
        final TextView name;
        //final TextView description;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
           // description = itemView.findViewById(R.id.description);
        }
    }
}
