package com.example.fifthPractical

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DatabaseHelper(
    context: Context
) : SQLiteOpenHelper(context, NAME_DB, null, VERSION) {

    companion object {
        const val COLUMN_ID: String = "id_book"
        const val COLUMN_NAME: String = "book_name"
        const val COLUMN_AUTHOR: String = "book_author"
        const val TABLE_NAME: String = "books"
        const val NAME_DB: String = "book_db"
        const val VERSION: Int = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val createTableQuery = """
            CREATE TABLE IF NOT EXISTS $TABLE_NAME (
                $COLUMN_ID INTEGER PRIMARY KEY AUTOINCREMENT,
                $COLUMN_NAME TEXT,
                $COLUMN_AUTHOR TEXT
            )
        """
        db?.execSQL(createTableQuery)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }

    fun addBook(bookName: String, bookAuthor: String) : Long {
        val db: SQLiteDatabase = this.writableDatabase

        val contentValue = ContentValues().apply {
            put(COLUMN_NAME, bookName)
            put(COLUMN_AUTHOR, bookAuthor)
        }

        return db.insert("books", null, contentValue)
    }

    fun getAllBooks(): Cursor? {
        val db: SQLiteDatabase = this.readableDatabase
        return db.query(TABLE_NAME, null, null, null, null, null, null)
    }

    fun getBookById(id: Long): Book? {
        val db: SQLiteDatabase = this.readableDatabase
        var book: Book? = null
        val cursor: Cursor = db.query(TABLE_NAME, null, "$COLUMN_ID = ?", arrayOf(id.toString()), null, null, null)
        if (cursor.moveToFirst()) {
            val bookId = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_ID))
            val bookName = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_NAME))
            val bookAuthor = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_AUTHOR))
            book = Book(bookId, bookName, bookAuthor)
        }
        cursor.close()
        return book
    }

    fun deleteBookById(id: Long): Int {
        val db: SQLiteDatabase = this.writableDatabase
        return db.delete(TABLE_NAME, "$COLUMN_ID = ?", arrayOf(id.toString()))
    }

    fun updateBookById(id: Long, bookName: String, bookAuthor: String): Int {
        val db: SQLiteDatabase = this.writableDatabase

        val contentValue = ContentValues().apply {
            put(COLUMN_NAME, bookName)
            put(COLUMN_AUTHOR, bookAuthor)
        }

        return db.update("books", contentValue, "$COLUMN_ID = ?", arrayOf(id.toString()))
    }
}