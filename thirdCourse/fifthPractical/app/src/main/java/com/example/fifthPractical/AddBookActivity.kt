package com.example.fifthPractical

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class AddBookActivity: AppCompatActivity() {
    private lateinit var editTextName: EditText
    private lateinit var editTextAuthor: EditText
    private lateinit var addButton: Button
    private lateinit var dbHelper: DatabaseHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_book)

        editTextName = findViewById(R.id.editTextName)
        editTextAuthor = findViewById(R.id.editTextAuthor)
        addButton = findViewById(R.id.add)

        dbHelper = DatabaseHelper(this)

        addButton.setOnClickListener{addBookToDatabase()}
    }

    private fun addBookToDatabase() {
        val bookName: String = editTextName.text.toString().trim()
        val bookAuthor: String = editTextAuthor.text.toString().trim()

        if (bookName.isEmpty() || bookAuthor.isEmpty()) {
            Toast.makeText(this, "Заполните все поля", Toast.LENGTH_SHORT).show()
            return
        }

        val result: Long = dbHelper.addBook(bookName, bookAuthor)

        if (result > 0) {
            Toast.makeText(this, "Книга добавлена", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        } else {
            Toast.makeText(this, "Ошибка добавления книги", Toast.LENGTH_SHORT).show()
        }
    }
}