package com.example.fifthPractical

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity

class DetailActivity : AppCompatActivity() {
    private lateinit var editTextName: EditText
    private lateinit var editTextAuthor: EditText
    private  lateinit var dbHelper: DatabaseHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_detail)

        dbHelper = DatabaseHelper(this)

        val idBook: Int = intent.getIntExtra("book_id", -1)
        val book: Book? = dbHelper.getBookById(idBook.toLong())
        editTextName = findViewById(R.id.editTextNameUpdate)
        editTextAuthor = findViewById(R.id.editTextAuthorUpdate)

        editTextName.setText(book?.bookName ?: "")
        editTextAuthor.setText(book?.bookAuthor ?: "")

        val updateBtn: Button = findViewById(R.id.update)
        updateBtn.setOnClickListener{ updateBookToDatabase(idBook) }

        val deleteBtn: Button = findViewById(R.id.delete)
        deleteBtn.setOnClickListener{ deleteBookToDatabase(idBook) }
    }

    private fun updateBookToDatabase(id: Int) {
        val bookName: String = editTextName.text.toString().trim()
        val bookAuthor: String = editTextAuthor.text.toString().trim()

        if (bookName.isEmpty() || bookAuthor.isEmpty()) {
            Toast.makeText(this, "Заполните все поля", Toast.LENGTH_SHORT).show()
            return
        }

        val result: Int = dbHelper.updateBookById(id.toLong(), bookName, bookAuthor)

        if (result > 0) {
            Toast.makeText(this, "Книга обновлена", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        } else {
            Toast.makeText(this, "Ошибка обновления книги", Toast.LENGTH_SHORT).show()
        }
    }

    private fun deleteBookToDatabase(id: Int) {
        val result: Int = dbHelper.deleteBookById(id.toLong())
        if (result > 0) {
            Toast.makeText(this, "Книга удалена", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        } else {
            Toast.makeText(this, "Ошибка удаления книги", Toast.LENGTH_SHORT).show()
        }
    }
}