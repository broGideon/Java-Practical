package com.example.fifthPractical

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewAdapter(private val itemList: List<Book>, private val onItemClick: (Book) -> Unit) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.book_card, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val book : Book = itemList[position]
        holder.bookAuthor.text = book.bookAuthor ?: "Unknown Author"
        holder.bookName.text = book.bookName ?: "Unknown Title"
        holder.itemView.setOnClickListener {
            onItemClick(book)
        }

    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder (itemView) {
        val bookAuthor : TextView = itemView.findViewById(R.id.b_author)
        val bookName : TextView = itemView.findViewById(R.id.b_name)
    }
}