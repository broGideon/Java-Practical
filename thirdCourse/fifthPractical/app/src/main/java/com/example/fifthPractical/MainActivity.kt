package com.example.fifthPractical

import android.content.Intent
import android.database.Cursor
import android.os.Bundle
import android.util.Log
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {
    private lateinit var dbHelper: DatabaseHelper
    private lateinit var bookList: ArrayList<Book>
    private lateinit var adapter: RecyclerViewAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.enableEdgeToEdge()
        setContentView(R.layout.activity_main)

        dbHelper = DatabaseHelper(this)
        bookList = arrayListOf()
        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(bookList) { item ->
            startActivity(Intent(this, DetailActivity::class.java).apply {
                putExtra("book_id", item.idBook)
                putExtra("book_name", item.bookName)
                putExtra("book_author", item.bookAuthor)
            })
        }
        recyclerView.adapter = adapter

        val fab: FloatingActionButton = findViewById(R.id.fab_add_book)
        fab.setOnClickListener { startActivity(Intent(this, AddBookActivity::class.java)) }

        loadBooks()
    }

    private fun loadBooks() {
        bookList.clear()
        val cursor: Cursor? = dbHelper.getAllBooks()
        if (cursor?.moveToFirst() == true) {
            do {
                val id: Int = cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_ID))
                val name: String = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_NAME))
                val author: String = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_AUTHOR))
                bookList.add(Book(id, name, author))
            } while (cursor.moveToNext())
        }

        cursor?.close()
        Log.d("LoadBooks", "Total books loaded: ${bookList.size}")
        adapter.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()
        loadBooks()
    }
}